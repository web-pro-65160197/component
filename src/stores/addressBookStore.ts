import { ref } from 'vue'
import { defineStore } from 'pinia'

interface AddreaaBook {
  id:number
  name: string
  tel: string
  gender: string
}

export const useAddressBookStore = defineStore('address_book', () => {

  const address = ref<AddreaaBook>({
    id:0 ,
    name: '' ,
    tel: '' ,
    gender: 'Male'
  })
  
  let lastId = 1
  
  const addressList = ref<AddreaaBook[]>([])
  const isAddNew = ref(false)
  
  function save(){
    if(address.value.id > 0) { //edit
      const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
      addressList.value[editedIndex] = address.value
    }else{ //Addnew
      addressList.value.push({...address.value, id: lastId++})
    }
    isAddNew.value = false
    address.value = {
      id:0 ,
      name: '' ,
      tel: '' ,
      gender: 'Male'
    }
  }
  
  function edit(id: number) {
    isAddNew.value = true
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]) ) 
  }
  
  function remove(id: number) {
    const removeIndex = addressList.value.findIndex((item) => item.id === id)
    addressList.value.splice(removeIndex, 1)
  }
  
  function cancle () {
    isAddNew.value = false
    address.value = {
      id:0 ,
      name: '' ,
      tel: '' ,
      gender: 'Male'
    }
  }

  return { address, addressList, save, isAddNew, edit, remove, cancle }
})
